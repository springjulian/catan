import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

import {
  Board,
  BoardServiceService,
  Game,
  Player,
  Road,
  Settlement,
  Tile,
} from 'src/app/services/board-service/board-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  playerID: string;

  players: Player[];
  board: Board;

  tilesRow1: Tile[] = [];
  tilesRow2: Tile[] = [];
  tilesRow3: Tile[] = [];
  tilesRow4: Tile[] = [];
  tilesRow5: Tile[] = [];

  constructor(private boardService: BoardServiceService,
              private matSnackBar: MatSnackBar) { }

  ngOnInit(): void {

    this.boardService.behaviourSub.subscribe((val: any) => {
      if (val.playerId) {
        this.playerID = val.playerId;
      }
      this.openSnackBar(`You received ${val}`, 'Close')
    });

    this.boardService.getNewGame().subscribe((game: Game) => {
      this.board = game.board;
      this.players = game.players;

      this.getRows();
    }, (error) => console.log(error));
  }

  openSnackBar(message: string, action: string) {
    this.matSnackBar.open(message, action, {
      duration: 2000,
    });
  }

  getRows() {

    let count = 0;
    const tiles = this.board.tiles;

    for (; count < 3; count++) {
      this.tilesRow1.push(tiles[count]);
    }

    for (; count < 7; count++) {
      this.tilesRow2.push(tiles[count]);
    }

    for (; count < 12; count++) {
      this.tilesRow3.push(tiles[count]);
    }

    for (; count < 16; count++) {
      this.tilesRow4.push(tiles[count]);
    }

    for (; count < 19; count++) {
      this.tilesRow5.push(tiles[count]);
    }
  }

  clickTile(tile: Tile): void {
    const roads: Road[] = this.getAssociatedRoad(tile);
    const settlements: Settlement[] = this.getAssociatedSettlements(tile);

    console.log(tile.id, settlements);
  }

  // Rows are between two tiles, finding the associated road for a tile
  private getAssociatedRoad(tile: Tile): Road[] {
    const roads = this.board.roads;
    return roads.map((road: Road) => {
      const tile1 = road.tileIds[0];
      const tile2 = road.tileIds[1];
      if (tile.id === tile1 || tile.id === tile2) {
        return road;
      }
    });
  }

  private getAssociatedSettlements(tile: Tile): Settlement[] {
    const settlements = this.board.settlements;
    return settlements.map((settlement: Settlement) => {
      const tile1 = settlement.tileIds[0];
      const tile2 = settlement.tileIds[1];
      const tile3 = settlement.tileIds[2];

      if (tile.id === tile1 || tile.id === tile2 || tile.id == tile3) {
        return settlement;
      }
    });
  }
}


