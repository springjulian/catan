import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';

export interface Game {
  board: Board;
  players: Player[];
  id: string;
}

export interface Player {
  id: string,
  color: string,
  piecesRemaining: string;
}

export interface Board {
  tiles: Tile[];
  roads: Road[];
  settlements: Settlement[]
}

export interface Tile {
  id: string;
  sides: Side[];
  resource: string;
  diceRoll: string;
  corners: Corner;
}

export interface Road {
  sideIds: string[];
  tileIds: string[];
  playerId: string;
}

export interface Settlement {
  cornerIds: string[];
  playerId: string;
  tileIds: string[];
  isCity: boolean;
}

export interface Side {
  id: string
  tileId: string
}

export interface Corner {
  id: string
  tileId: string
}

@Injectable({
  providedIn: 'root',
})
export class BoardServiceService {

  myWebSocket: WebSocketSubject<any> = webSocket('ws://localhost:8080/ws');

  behaviourSub = new BehaviorSubject<any>({} as any);

  constructor(private http: HttpClient) {
    this.myWebSocket.subscribe((event: any) => {
        console.log(event);
        this.behaviourSub.next(event);
      },
      (error) => console.log(error),
    );

  }

  getNewGame(): Observable<any> {
    return this.http.get('http://localhost:8080/getnewgame');
  }


}
