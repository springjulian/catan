package game

import (
  "catan-server/db"

  "strconv"
  )

func GetDistributeResources(t db.Tile) []string {
  // player to distribute to

  // find settlements that are part of the t tile
  // find the player id from the Settlement

  // Dummy set up for broadcast testing
  var sides []db.Side
  for i := 0; i <= 5; i++ {
    sides = append(sides, db.Side{
      Id:     "0" + strconv.Itoa(i),
      TileID: "0",
    })
  }

  var corners []db.Corner
  for i := 0; i <= 5; i++ {
    corners = append(corners, db.Corner{
      Id:     "0" + strconv.Itoa(i),
      TileID: "0",
    })
  }

  tile := db.Tile{
    Id:       "0",
    Sides:    sides,
    Resource: "Forest",
    DiceRoll: "6",
    Corners:  corners,
  }

  t = tile

  for _, cid := range t.Corners {
    // distibute to these players
    return getSettlementsPlayerId(cid.Id)
  }
  return []string{""}
}

func getSettlementsPlayerId(cid string) []string {

  var ss []db.Settlement
  var ns []string

  ss = append(ss, db.Settlement{
    CornerId: [3]string{"04", "32", "40"},
    PlayerId: "1",
    TileIds:  [3]string{"0", "3", "4"},
  })

  for _, s := range ss {
    for _, c := range s.CornerId {
      if c == cid {
        ns = append(ns, cid)
      }
    }
  }

  return ns
}
