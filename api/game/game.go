package game

import (
  "catan-server/db"

  "encoding/json"
  "net/http"
  "strconv"
  "fmt"
)

func GetNewGame(w http.ResponseWriter, r *http.Request) {

  w.WriteHeader(http.StatusOK)
  err := json.NewEncoder(w).Encode(makeNewGame())
  if err != nil {
    w.WriteHeader(http.StatusInternalServerError)
    fmt.Print(err)
  }
}

func makeNewGame() db.Game {

  newBord := makeNewBoard()

  return db.Game{
    Players: nil,
    Board:   newBord,
    Id:      "newGameId",
  }

}

func makeNewBoard() db.Board {
  diceRoll := getDiceRoll()
  resources := getResouces()

  var tiles []db.Tile
  var roads []db.Road
  var settlements []db.Settlement

  for i := 0; i <= 18; i++ {
    id := strconv.Itoa(i)
    tile := makeTile(id, diceRoll[i], resources[i])

    tiles = append(tiles, tile)
  }
  // TODO: get the roads from the with gameID -> this is for testing
  roads = append(roads, db.Road{
    SideIds:  [2]string{"02", "15"},
    PlayerId: "1",
    TileIds:  [2]string{"0", "1"},
  })

  settlements = append(settlements, db.Settlement{
    CornerId: [3]string{"04", "32", "40"},
    PlayerId: "1",
    TileIds:  [3]string{"0", "3", "4"},
  })

  return db.Board{
    Tiles:       tiles,
    Roads:       roads,
    Settlements: settlements,
  }

}

func makeTile(id string, dR string, r string) db.Tile {

  sides := getSides(id)
  corners := getCorners(id)

  return db.Tile{
    Id:       id,
    DiceRoll: dR,
    Resource: r,
    Sides:    sides,
    Corners:  corners,
  }

}

func getSides(id string) []db.Side {
  var sides []db.Side
  for i := 0; i <= 5; i++ {
    side := db.Side{
      Id:     id + strconv.Itoa(i),
      TileID: id,
    }
    sides = append(sides, side)
  }

  return sides
}

func getCorners(id string) []db.Corner {
  var corners []db.Corner
  for i := 0; i <= 5; i++ {
    corner := db.Corner{
      Id:     id + strconv.Itoa(i),
      TileID: id,
    }
    corners = append(corners, corner)
  }

  return corners
}

func getDiceRoll() [19]string {
  return [19]string{
    "2", "3", "3", "4", "4", "5", "5", "6", "6", "8", "8", "9", "9", "10", "10", "11", "11", "12", "none",
  }
}

func getResouces() [19]string {
  return [19]string{
    "Forest", "Forest", "Forest", "Forest", "Pasture", "Pasture", "Pasture", "Pasture",
    "Fields", "Fields", "Fields", "Fields", "Hills", "Hills", "Hills", "Mountain", "Mountain", "Mountain", "Desert",
  }
}

func getChips() [19]string {
  return [19]string{
    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "none",
  }
}
