package main

import (
  "catan-server/db"
  "catan-server/game"
  "encoding/json"
  "fmt"
  "io/ioutil"
  "log"
  "net/http"
  "strconv"

  "github.com/gorilla/handlers"
  "github.com/gorilla/mux"
  "github.com/gorilla/websocket"
)

type initResp struct {
  ID string `json:"playerId"`
}

type Client struct {
  Conn *websocket.Conn
  ID   string
}

type ResourceDist struct {
  PlayerId string `json:"playerId"`
  Resource string `json:"resource"`
}

var clientsWithId = make(map[*websocket.Conn]Client)
var clients = make(map[*websocket.Conn]bool)
var broadcast = make(chan *ResourceDist)
var upgrader = websocket.Upgrader{
  CheckOrigin: func(r *http.Request) bool {
    return true
  },
}

func longLatHandler(w http.ResponseWriter, r *http.Request) {
  var rd ResourceDist
  if err := json.NewDecoder(r.Body).Decode(&rd); err != nil {
    log.Printf("ERROR: %s", err)
    http.Error(w, "Bad request", http.StatusTeapot)
    return
  }

  defer r.Body.Close()
  _, err := ioutil.ReadAll(r.Body)
  if err != nil {
    log.Printf("ERROR: %s", err)
  }
  go writer(&rd)
}

func writer(rd *ResourceDist) {
  broadcast <- rd
}

func wsHandler(w http.ResponseWriter, r *http.Request) {
  fmt.Println("Client connecting")
  ws, err := upgrader.Upgrade(w, r, nil)
  if err != nil {
    log.Fatal(err)
  }



  newClient := Client{
    ID:   strconv.Itoa(len(clientsWithId) + 1),
    Conn: ws,
  }
  fmt.Println("ID", strconv.Itoa(len(clientsWithId)+1))

  err = ws.WriteJSON(initResp{ID: strconv.Itoa(len(clientsWithId) + 1)})
  if err != nil {
    log.Fatal(err)
  }
  clientsWithId[ws] = newClient
}

func echo() {
  for {
    val := <-broadcast
    resource := fmt.Sprintf("%s", val.Resource)
    for _, clientWithId := range clientsWithId {
      if clientWithId.ID != val.PlayerId {
        continue
      }
      err := clientWithId.Conn.WriteJSON(resource)
      if err != nil {
        log.Printf("Websocket error: %s", err)
        clientWithId.Conn.Close()
      }
    }
  }
}

func main() {
  r := mux.NewRouter()
  r.HandleFunc("/", HelloServer)
  r.HandleFunc("/getnewgame", game.GetNewGame).Methods("GET")
  r.HandleFunc("/test", longLatHandler).Methods("POST")
  r.HandleFunc("/ws", wsHandler)

  db.ConnectDb()
  go echo()

  log.Fatal(http.ListenAndServe(":8080", handlers.CORS()(r)))
}

func HelloServer(w http.ResponseWriter, r *http.Request) {
  fmt.Fprintf(w, "Hello, %s!", r.URL.Path[1:])
}
