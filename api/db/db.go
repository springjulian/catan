package db

import (
  "fmt"

  "github.com/go-redis/redis"
)

type Game struct {
  Players []Player `json:"players"`
  Board   Board    `json:"board"`
  Id      string   `json:"id"`
}



func ConnectDb() {

  client := redis.NewClient(&redis.Options{
    Addr: "localhost:6379",
    Password: "",
    DB: 0,
  })

  pong, err := client.Ping().Result()
  fmt.Println(pong, err)
}

