package db

type Player struct {
  Id             string `json:"id"`
  Color          string `json:"color"`
  PieceRemaining string `json:"pieceRemaining"`
}

type Board struct {
  Tiles       []Tile       `json:"tiles"`
  Roads       []Road       `json:"roads"`
  Settlements []Settlement `json:"settlements"`
}

type Tile struct {
  Id       string   `json:"id"`
  Sides    []Side   `json:"sides"`
  Corners  []Corner `json:"corner"`
  Resource string   `json:"resource"`
  DiceRoll string   `json:"diceRoll"`
}

type Road struct {
  SideIds  [2]string `json:"sideIds"`
  PlayerId string    `json:"playerId"`
  TileIds  [2]string `json:"tileIds"`
}

type Settlement struct {
  CornerId [3]string `json:"cornerIds"`
  PlayerId string    `json:"playerId"`
  TileIds  [3]string `json:"tileIds"`
  IsCity   bool      `json:"isCity"`
}

type Side struct {
  Id     string `json:"id"`
  TileID string `json:"tileId"`
}

type Corner struct {
  Id     string `json:"id"`
  TileID string `json:"tileId"`
}